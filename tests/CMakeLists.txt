cmake_minimum_required(VERSION 2.8.12)
project(GameSparksTests)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

if(APPLE)
	# ugly workaround for Version 7.0
	set(CMAKE_OSX_SYSROOT "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.11.sdk")
endif()

add_subdirectory (gtest ${CMAKE_BINARY_DIR}/bin/gtest)
add_subdirectory (../base/build_scripts/ ${CMAKE_BINARY_DIR}/bin/gs)

file(GLOB_RECURSE TEST_SOURCES RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_SOURCE_DIR}/base/*.cpp")

add_executable(
    ${PROJECT_NAME}
    ${TARGET_EXTRA_ARG}
    ${TEST_SOURCES}
    "${CMAKE_CURRENT_SOURCE_DIR}/../base/samples/extra/usleep.cpp"
)

target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../base/samples/extra")
target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../base/include")
target_link_libraries(${PROJECT_NAME} gtest GameSparks ${WS_LIBS})

if(IOS)
	set_target_properties(${PROJECT_NAME} PROPERTIES XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY "iPhone Developer: Benjamin Schulz")
    TARGET_LINK_LIBRARIES (${PROJECT_NAME} "-framework UIKit")
    set_xcode_property (${PROJECT_NAME} IPHONEOS_DEPLOYMENT_TARGET "7.0")
endif()

if (APPLE)
	TARGET_LINK_LIBRARIES (${PROJECT_NAME} "-framework Foundation")
endif()
